import Vue from "vue";
import Component from "vue-class-component";
import Position from "@/interfaces/iSpace";

@Component
export default class BaseItem extends Vue {
  x!: number;
  y!: number;
  pos!: Position;

  isMouseOver: boolean = false;
  isMouseDown: boolean = false;

  mounted() {
    const onMouseDown = (e: MouseEvent) => {
      e.preventDefault();
      this.isMouseDown = true;
    };

    const onDrag = (e: any | MouseEvent) => {
      e.preventDefault();
      if (this.isMouseDown && this.isMouseOver) {
        console.log(e);
        this.pos = {
          x: e.clientX - (e.offsetX - e.movementX),
          y: e.clientY - (e.offsetY - e.movementY)
        };
        // this.pos = {
        //   x: e.clientX + e.movementX - e.offsetX,
        //   y: e.clientY + e.movementY - e.offsetY
        // };
      }
    };

    const onMouseUp = (e: MouseEvent) => {
      this.isMouseDown = false;
    };

    this.$el.addEventListener("mouseover", () => (this.isMouseOver = true));
    this.$el.addEventListener("mouseout", () => {
      this.isMouseOver = false;
      this.isMouseDown = false;
    });
    this.$el.addEventListener("mousedown", e => onMouseDown(e as MouseEvent));

    this.$el.addEventListener("mousemove", e => onDrag(e as MouseEvent));
    this.$el.addEventListener("mouseup", e => onMouseUp(e as MouseEvent));
  }

  // dragStart(e: MouseEvent) {
  //   e.preventDefault();
  //   this.oldPos = {
  //     x: e.clientX,
  //     y: e.clientY
  //   };
  // }

  // dragging(e: any) {
  //   e.preventDefault();
  //   this.pos = {
  //     x: e.clientX - e.layerX - (this.oldPos.x - e.clientX),
  //     y: e.clientY - e.layerY - (this.oldPos.y - e.clientY)
  //   };
  //   this.oldPos = {
  //     x: e.clientX,
  //     y: e.clientY
  //   };
  //   console.log(e);
  // }
}
