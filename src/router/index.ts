import Vue from "vue";
import VueRouter from "vue-router";
import Space from "@/views/Space.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Space",
    component: Space
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
