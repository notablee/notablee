import { shallowMount } from "@vue/test-utils";
import { expect } from "chai";
import Textbox from "@/components/Textbox.vue";

describe("Textbox.vue", () => {
  it("should render the passed text", () => {
    const text = "Renders text";
    const wrapper = shallowMount(Textbox, {
      propsData: {
        text: text
      }
    });
    expect(wrapper.text()).to.equal(text);
  });
  it("should return the right position", () => {
    const pos = {
      x: 60,
      y: 50
    };
    const wrapper = shallowMount(Textbox, {
      propsData: {
        ...pos
      }
    });
    expect(wrapper.vm.pos.x)
      .to.be.a("number")
      .and.equals(pos.x);
    expect(wrapper.vm.pos.y)
      .to.be.a("number")
      .and.equals(pos.y);
  });
});
